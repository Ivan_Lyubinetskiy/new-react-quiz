import axios from 'axios'

export default axios.create({
  baseURL: 'https://react-quiz-50d28.firebaseio.com/'
})
