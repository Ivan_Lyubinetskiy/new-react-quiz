//компонент(функция), в котором содержаться все ответы;
import React from 'react';
import classes from './AnswersList.css';
import AnswerItem from './AnswerItem/AnswerItem';
//в props получаем массив answer
//answer будут храниться в отдельном компаненте
//круглые скобки потому, что выводим сразу jsx
const AnswersList = props => (
  <ul className={classes.AnswersList}>
    {props.answers.map((answer, index)=>{
      return(
        <AnswerItem
          key={index}
          answer={answer}
          onAnswerClick={props.onAnswerClick}
          state={props.state ? props.state[answer.id] : null}
        />
      )
    })}
  </ul>

)


export default AnswersList;
