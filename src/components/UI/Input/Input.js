import React from 'react'
import classes from './Input.css'

//деструктаризируем объект props, из которого нужны только valid, touched, shoudValidat
function isInvalid({valid, touched, shouldValidate}) {
  //если не валедированный, если должны валедировать, если потрокали означает, что невалидный
  return !valid && shouldValidate && touched
}

const Input = props => {
  //определяет тип input
  const inputType = props.type || 'text'
  const cls = [
    classes.Input
  ]
  const htmlFor = `${inputType}-${Math.random()}`
  //проверка на валидацию если в функцию isInvalid возвращаються {valid, touched, shoudValidate}, то добавляем класс
  if (isInvalid(props)) {
    cls.push(classes.invalid)
  }

  return (
    <div className={cls.join(' ')}>
    <label htmlFor={htmlFor}>{props.label}</label>
      <input
        type={inputType}
        id={htmlFor}
        value={props.value}
        onChange={props.onChange}
      />
      {isInvalid(props) ? <span>{props.errorMessage || 'Введите верное значение'}</span> : null}
    </div>
  )
}


export default Input
