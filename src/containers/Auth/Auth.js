import React, {Component} from 'react'
import classes from './Auth.css'
import Button from '../../components/UI/Button/Button'
import Input from '../../components/UI/Input/Input'
import is from 'is_js'
import {connect} from 'react-redux';
import {auth} from '../../store/actions/auth';

class Auth extends Component {
//state нужен для форм контролов
  state = {
    isFormValid: false,
    formControls: {
      email: {
        value: '',
        type: 'email',
        label: 'Email',
        errorMessage: 'Введите корректный email',
        valid: false,
        touched: false,
        validation: {
          required: true,
          email: true
        }
      },
      password: {
        value: '',
        type: 'password',
        label: 'Пароль',
        errorMessage: 'Введите корректный пароль',
        valid: false,
        touched: false,
        validation: {
          required: true,
          minLength: 6
        }
      }
    }
  }

  loginHandler = () => {

    this.props.auth(
      this.state.formControls.email.value,
      this.state.formControls.password.value,
    true)
  }

  registerHandler = () => {
    this.props.auth(
      this.state.formControls.email.value,
      this.state.formControls.password.value,
    false)

  }

  submitHandler = event => {
    event.preventDefault()
  }

  validateControl(value, validation) {
    if (!validation) {
      return true
    }

    let isValid = true

    if (validation.required) {
//если не равна пустой строке и ранее не была false то останется значение true
      isValid = value.trim() !== '' && isValid
    }

    if (validation.email) {
      isValid = is.email(value) && isValid
    }

    if (validation.minLength) {
      isValid = value.length >= validation.minLength && isValid
    }

    return isValid
  }
 //передаем событие и controlName, чтобы понять какой контрол мы редактируем
  onChangeHandler = (event, controlName) => {
    const formControls = { ...this.state.formControls }
  //в const control находиться либо email либо password со всеми полями для валидации
    const control = { ...formControls[controlName] }

    control.value = event.target.value
  //как только поподаю в эту функцию изменяем touched на true
    control.touched = true
      //в фун-ю передаем новое значения и объект control.validation чтоб понять как правильно валидировать
    control.valid = this.validateControl(control.value, control.validation)
//изменяем локальный formControls
    formControls[controlName] = control

    let isFormValid = true

    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid && isFormValid
    })

    this.setState({
      formControls, isFormValid
    })
  }
// возвращает массив из inputs
  renderInputs() {
    // в переменную получаем, с помощью (просто возвращаем) Object.keys получаем массив из объекта formControls (массив из email и password)
    // через map перебераем массив и передаем сallback fn(возвращает input с значениями)
    // const inputs = Object.keys(this.state.formControls).map((controlName, index) => {
    return Object.keys(this.state.formControls).map((controlName, index) => {
        // переменная определяеться по ключу controlName
      const control = this.state.formControls[controlName]
            // !! преобразует значение после в булевое и возвращает true если это возможно, в противном случае false
      return (
        <Input
          key={controlName + index}
          type={control.type}
          value={control.value}
          valid={control.valid}
          touched={control.touched}
          label={control.label}
          shouldValidate={!!control.validation}
          errorMessage={control.errorMessage}
          onChange={event => this.onChangeHandler(event, controlName)}
        />
      )
      // возвращаем переменную input
      // return inputs
    })
  }

  render() {
    return (
      <div className={classes.Auth}>
        <div>
          <h1>Авторизация</h1>

          <form onSubmit={this.submitHandler} className={classes.AuthForm}>

            { this.renderInputs() }

            <Button
              type="success"
              onClick={this.loginHandler}
              disabled={!this.state.isFormValid}
            >
              Войти
            </Button>

            <Button
              type="primary"
              onClick={this.registerHandler}
              disabled={!this.state.isFormValid}
            >
              Зарегистрироваться
            </Button>
          </form>
        </div>
      </div>
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    auth: (email, password, isLogin) => dispatch(auth(email, password, isLogin))
  }
}

export default connect(null, mapDispatchToProps)(Auth)
