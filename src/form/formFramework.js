export function createControl(config, validation) {
  return {
    ...config,
    validation,
    // начальные значения valid будут !validation - false, не равен переданым валидациям
    valid: !validation,
    touched: false,
    value: ''
  }
}

export function validate(value, validation = null) {
  if (!validation) {
    return true
  }

  let isValid = true

  if (validation.required) {
    isValid = value.trim() !== '' && isValid
  }

  return isValid
}

export function validateForm(formControls) {
  let isFormValid = true
//пройтись по formControls если валидные все елементы то и форма валидная
  for (let control in formControls) {
    //проверяем конкретно те поля которые вписывали в state
    if (formControls.hasOwnProperty(control)) {
      isFormValid = formControls[control].valid && isFormValid
    }
  }

  return isFormValid
}
