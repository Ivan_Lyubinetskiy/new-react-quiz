import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import {BrowserRouter} from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker'
import {createStore, compose, applyMiddleware} from 'redux'
//Provider нужен для соединения реакт и редакс, работает как higher-order component
import {Provider} from 'react-redux'
import rootReducer from './store/reducers/rootReducer'
//для работы с асинхронным кодом (Middleware)
import thunk from 'redux-thunk'

const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    }) : compose;
//создали стор и через фуекцию createStore передаем rootReducer, composeEnhancers необхадима для redux-DevTools, в которую необходимо положить все Middleware
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))
const app =(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
