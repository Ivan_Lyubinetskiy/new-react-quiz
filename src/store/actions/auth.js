import axios from 'axios'
import {AUTH_SUCCESS, AUTH_LOGOUT} from './actionTypes';

export function auth(email, password, isLogin) {
  return async dispatch =>{
    const authData = {
      email,
      password,
      returnSecureToken: true
    }

      let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyDJ5V34WcBX4kKqV7s24PjGMzbZ-L8RSNM'

      if (isLogin) {
        url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyDJ5V34WcBX4kKqV7s24PjGMzbZ-L8RSNM'
      }

      const response = await axios.post(url, authData)
      const data = response.data


      const expirationData = new Date(new Date().getTime() + data.expiresIn * 1000)
// для сессии
      localStorage.setItem('token', data.idToken)
      localStorage.setItem('userId', data.localId)
      localStorage.setItem('expirationData', expirationData)

      dispatch(authSuccess(data.idToken))
      dispatch(authLogout(data.expiresIn))
  }
}

export function authSuccess(token){
  return{
    token,
    type: AUTH_SUCCESS
  }
}

export function authLogout(time){
  return dispatch => {
    setTimeout(()=>{
      dispatch(logout)
    }, time * 1000)
  }
}

export function logout(){
  localStorage.removeItem('token')
  localStorage.removeItem('userId')
  localStorage.removeItem('expirationData')
  return{
    type: AUTH_LOGOUT
  }
}

export function autoLogin(){
  return dispatch =>{
    const token = localStorage.getItem('token')
    if(!token){
      dispatch(logout())
    } else {
      const expirationData = new Date(localStorage.getItem('expirationData'))
      if (expirationData <= new Date()) {
        dispatch(logout())
      } else {
        dispatch(authSuccess(token))
        dispatch(authLogout((expirationData.getTime() - new Date().getTime()) / 1000 ))
      }
    }
  }
}
